@extends("layout")

@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        Sửa căn hộ
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('canho.index') }}" class="btn btn-primary float-end">Thêm căn hộ</a></div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('canho.update', $canho->id)}}" method="POST">
                    @csrf
                    @method("PUT")
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Mã chủ sở hữu</strong>
                                <input type="text" name="MaCSH" class="form-control" placeholder="Nhập mã chủ sở hữu ">
                            </div>
                            <div class="form-group">
                                <strong>Họ và tên chủ sở hữu</strong>
                                <input type="text" name="TenChu" value="{{$canho->TenChu}}" class="form-control" placeholder="Nhập họ và tên">
                            </div> 
                            <div class="form-group">
                                <strong>Gía</strong>
                                <input type="number" name="Gia" value="{{$canho->Gia }}" class="form-control" placeholder="Nhập Gía căn hộ">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Cập nhật</button>
                </form>
            </div>
        </div>
    </div>
@endsection