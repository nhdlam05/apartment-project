@extends('layout')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">   
                    <div class="col-md-6">Quản lý căn hộ</div>
                    <div class="col-md-6">
                        <a href="{{route('canho.create') }}" class="btn btn-primary float-end">Thêm căn hộ</a></div>
                </div>
            </div>
            <div class="card-body">
                @if(Session::has('thongbao'))
                    <div class="alert alert-success">
                        {{ Session::get("thongbao") }}
                    </div>
                @endif
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Mã căn hộ</th>
                            <th>Tên chủ sở hữu</th>
                            <th>Tình trạng</th>
                            <th>Gía</th>
                            <th>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($canho as $ch)
                            <tr>
                                <td>{{ $ch ->MaCH }}</td>
                                <td>{{ $ch ->TenChu }}</td>
                                <td>{{ $ch ->TinhTrang }}</td>
                                <td>{{ $ch ->Gia }}</td>
                                <td>
                                    <form action="{{route('canho.destroy', $ch->id) }}" method="POST">
                                        <a href="{{route('canho.edit', $ch->id) }}" class="btn btn-info">Sửa</a>
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="btn btn-danger">Xóa</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection