@extends("layout")

@section("content")
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        Thêm căn hộ
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('canho.index') }}" class="btn btn-primary float-end">Thêm căn hộ</a></div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('canho.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <strong>Mã căn hộ</strong>
                                <input type="text" name="MaCH" class="form-control" placeholder="Nhập mã căn hộ">
                            </div>
                            
                            <div class="form-group">
                                <strong>Mã Hóa Đơn</strong>
                                <input type="text" name="MaHD" class="form-control" placeholder="Nhập mã hóa đồng ">
                            </div>
                            <div class="form-group">
                                <strong>Mã chủ sở hữu</strong>
                                <input type="text" name="MaCSH" class="form-control" placeholder="Nhập mã chủ sở hữu    ">
                            </div>
                            <div class="form-group">
                                <strong>Họ và tên chủ sở hữu</strong>
                                <input type="text" name="TenChu" class="form-control" placeholder="Nhập họ và tên">
                            </div>
                            <div class="form-group">
                                <strong>Gía</strong>
                                <input type="number" name="Gia" class="form-control" placeholder="Nhập Gía căn hộ">
                            </div>
                            <div class="form-group">
                                <strong>Tang</strong>
                                <input type="number" name="Tang" class="form-control" placeholder="Nhập Tầng căn hộ">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <strong>Tình trạng</strong>
                            <select name="TinhTrang" id="" class="form-select">
                                <option value="selected">Chọn tình trạng</option>
                                <option value="Mới">Mới</option>
                                <option value="Cũ">Cũ</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Lưu</button>
                </form>
            </div>
        </div>
    </div>
@endsection