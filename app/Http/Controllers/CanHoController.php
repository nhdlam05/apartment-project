<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CanHo;
class CanHoController extends Controller
{
    public function index() {
        $canho = CanHo::paginate(5);
        return view('index',compact("canho")) -> with("i", (request() -> input("page",1)-1) * 5);
    }
    public function create() {
        return view('create');
    }
    public function store(Request $request) {
        CanHo::create($request-> all());
        return redirect()-> route("canho.index")->with("thongbao","Thêm căn hộ thành công");
    }
    public function edit(CanHo $canho) {
        return view('edit',compact('canho'));
    }
    public function update(Request $request, CanHo $canho) {
        $canho ->update($request->all());
        return redirect()-> route('canho.index')-> with('thongbao', 'Cập nhật thành công! ');
    }

    public function destroy(CanHo $canho) {
        $canho ->delete();
        return redirect()-> route('canho.index')->with("thongbao","Xóa thành công! ");
    }
}