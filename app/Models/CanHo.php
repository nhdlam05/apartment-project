<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanHo extends Model
{
    use HasFactory;
    protected $fillable = [
        "MaCH",
        "TinhTrang",
        "MaCSH",
        "Gia",
        "Tang",
        "MaHD",
        "TenChu"
    ];
}
