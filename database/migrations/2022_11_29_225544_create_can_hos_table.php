<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use SebastianBergmann\CodeUnit\FunctionUnit;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('can_hos', function (Blueprint $table) {
            $table->id();
            $table->string("MaCH");
            $table->string("TinhTrang");
            $table->string("MaCSH");
            $table->integer("Gia");
            $table->string("Tang");
            $table->string("MaHD");
            $table->string("TenChu");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('can_hos');
    }
};
